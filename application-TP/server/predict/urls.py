from django.urls import path

from . import views

app_name = "predict"

urlpatterns = [
    path('', views.index, name='index'),
    path('api/', views.predict, name='submit_prediction'),
    path('results/', views.view_results, name='results'),
]
