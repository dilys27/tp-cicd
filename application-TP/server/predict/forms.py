from django import forms


class IrisForm(forms.Form):
    sepal_length = forms.FloatField(label='Sepal length')
    sepal_width = forms.FloatField(label='Sepal Width')
    petal_length = forms.FloatField(label='Petal length')
    petal_width = forms.FloatField(label='Petal Width')
