from django.db import models

# Create your models here.
class IrisData(models.Model):

    sepal_length = models.FloatField()
    sepal_width = models.FloatField()
    petal_length = models.FloatField()
    petal_width = models.FloatField()
    # par défaut l'espece est définie a unknown
    species = models.CharField(max_length=30, default='unknown')

    def __str__(self):
        return self.species

    def _copy_iris(self, source_iris_data):
        self.sepal_length = source_iris_data.sepal_length
        self.sepal_width = source_iris_data.sepal_width
        self.petal_width = source_iris_data.petal_width
        self.petal_width = source_iris_data.petal_width
        self.species = source_iris_data.species
        

