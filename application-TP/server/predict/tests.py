from django.test import TestCase

# Create your tests here.

from .models import IrisData

class IrisDataTestCase(TestCase):

    def tests_models(self):
        """
        Gestion des modèles voir les methodes dans models.py
        """
        iris1 = IrisData()
        # Verifier la fonction print __str__()
        self.assertEqual(iris1.__str__(), 'unknown')
        # Verifier la fonction copy _copy_iris()
        self.assertEqual(iris1._copy_iris(iris1), iris1)

    """
    Gestion des redirections voir les redirections dans server/urls.py et server/predict.urls.py
    """
    # du code ici
    
    """
    Gestion avec la base de données: vérifier que lors d'un save() la donnée et bien disponible dans le BDD
    """
    # du code ici
    
    ## en bonus, vous pouvez vérifier le forms.py