from django.shortcuts import render
import joblib

from .models import IrisData
from .forms import IrisForm
# Create your views here.

def index(request):
    return render(request, 'predict/index.html')

def predict(request):
    if request.method == 'POST':

        iris_form = IrisForm(request.POST)
        if iris_form.is_valid():
            sepal_length = float(request.POST.get('sepal_length'))
            sepal_width = float(request.POST.get('sepal_width'))
            petal_length = float(request.POST.get('petal_length'))
            petal_width = float(request.POST.get('petal_width'))

            model = joblib.load('../../research/IrisRandomForestClassifier.joblib')

            result = model.predict([[sepal_length, sepal_width, petal_length, petal_width]])

            print(result)
            species = result[0]

            IrisData.objects.create(sepal_length=sepal_length, sepal_width=sepal_width, petal_length=petal_length,
                                       petal_width=petal_width, species=species)
    else:
        iris_form = IrisForm()
        species=""

    context = {'form': iris_form, 'specie' : species}
    return render(request, 'predict/predict.html', context)



def view_results(request):
    data = {"dataset": IrisData.objects.all()}
    return render(request, "predict/results.html", data)
