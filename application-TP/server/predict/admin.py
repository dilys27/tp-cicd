from django.contrib import admin

# Register your models here.

# pour faire apparaitre le modèle dans la partie admin
from .models import IrisData
admin.site.register(IrisData)
